'use strict';

(function(root) {

	// svg for all
	svg4everybody();

	// Global
	var winWidth = $(window).width();

	// Sliders
	$('.slider__list').owlCarousel({
		items: 1,
		loop: true,
		smartSpeed: 400,
		autoplay: true,
		autoplayTimeout: 5000,
		nav: true,
		navText: [
			'<svg><use xlink:href="img/symbols.svg#svg-arrow"/></svg>',
			'<svg><use xlink:href="img/symbols.svg#svg-arrow"/></svg>'],
	});

	$('.choose__item').on('click', function() {
		var price1 = $(this).data('price1'),
				price2 = $(this).data('price2'),
				choose = $(this).closest('.choose');

		if (!price1) {
			return false;
		}

		$(choose).find('.choose__item_active').removeClass('choose__item_active');

		$(this).addClass('choose__item_active');

		$(choose).find('.choose__value[data-price1] strong').text(price1);
		$(choose).find('.choose__value[data-price2] strong').text(price2);
	});

	$('.choose').each(function(i, choose) {
		$(choose).find('.choose__item').first().trigger('click');
	});

	$('[data-open]').on('click', function() {
		var data = $(this).data('open'),
				toggle = $(this).find('[data-open-toggle]').get(0);

		if (toggle) {
			$(toggle).find('svg,img').toggle();
		}

		$('[data-show=' + data + ']').toggle();
	});

	$('.menu').on('click', function() {
		var h = $('.header__wrap').outerHeight() + $('.header').outerHeight();

		if ($('.page_freeze').get(0)) {
			$('.page').removeClass('page_freeze').css('height', '');
			return false;
		}

		$('.page').addClass('page_freeze').css('height', h);
	});

	if ($('.header_index').get(0)) {

		$(window).on('scroll', function() {
			if (document.documentElement.clientWidth > 1000) {
				var h = $('.slider').height() + $('.header').height();

				var scroll = $(window).scrollTop(),
						margin = scroll / 2.5;

				if (scroll >= h) {
					$('.logotype').css('margin-top', '-500px');
					setTimeout(function() {
						$('.header').removeClass('header_index');
					}, 100);
				}
				else {
					$('.logotype').css('margin-top', '-' + margin + 'px');
					$('.header').addClass('header_index');
				}
			}
			else {
				$('.header').removeClass('header_index');
			}
		});

		if (document.documentElement.clientWidth < 1000) {
			$('.header').removeClass('header_index');
		}
	}
	;
	$(document).on('click', '[data-show-more]', function() {
		var btn = $(this);
		var page = btn.attr('data-next-page');
		var id = btn.attr('data-show-more');
		var bx_ajax_id = btn.attr('data-ajax-id');

		var data = {
			bxajaxid: bx_ajax_id,
		};
		data['PAGEN_' + id] = page;

		$.ajax({
			type: 'GET',
			url: window.location.href,
			data: data,
			timeout: 3000,
			success: function(data) {
				var $data = $(data);
				$('#btn_' + bx_ajax_id).remove();

				var $area = $('.js-grid');
				$area.append($data);

			},
		});
	});
})(window);
