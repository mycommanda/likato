<?php

namespace Letsrock\Lib\Models;

use Bitrix\Main\Loader;

Loader::includeModule('iblock');
Loader::includeModule('sale');
/*
 * Class Basket
 * Класс для работы с товарами в корзине
 */

class Basket
{


    /**
     * Метод получения данных в корзине
     * @param null $order
     * @param string $delay
     * @return array
     */
    public static function getBasket(
        $type = false,
        $order = null,
        $delay = 'N',
        $select = array(
            'ID',
            'CALLBACK_FUNC',
            'MODULE',
            'PRODUCT_ID',
            'QUANTITY',
            'DELAY',
            'CAN_BUY',
            'PRICE',
            'WEIGHT'
        ),
        $sort = array(
            'NAME' => 'ASC',
            'ID' => 'ASC'
        )

    ) {
        $arBasketItems = array();
        $dbBasketItems = \CSaleBasket::GetList(
            $sort,
            array(
                'FUSER_ID' => \CSaleBasket::GetBasketUserID(),
                'LID' => SITE_ID,
                'ORDER_ID' => $order,
                'DELAY' => $delay
            ),
            false,
            false,
            $select
        );

        $i = 0;
        $cartCount = null;
        $cartSum = null;
        while ($arItems = $dbBasketItems->Fetch()) {

            $arBasketItems['ITEMS'][$i] = $arItems;

            if($type === true)
                $arBasketItems['ITEMS'][$i]['DETAIL'] = \CIBlockElement::GetByID($arItems['PRODUCT_ID'])->GetNext();

            $cartCount += $arItems['QUANTITY'];
            $cartSum += $arItems['PRICE'] * $arItems['QUANTITY'];
            $i++;
        }

        if (!empty($cartCount))
            $arBasketItems['COUNT'] = $cartCount;
        else
            $arBasketItems['COUNT'] = 0;

        if (!empty($cartSum))
            $arBasketItems['SUM'] = $cartSum;
        else
            $arBasketItems['SUM'] = 0;

        return $arBasketItems;

    }

    /**
     * Метод добавления товара в корзину
     * @param int $productId
     * @param int $quantity
     * @return bool
     */
    public static function add(
        $productId,
        $quantity = 1
    ) {

        if(empty($quantity))
            $quantity = 1;

        $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(),
            \Bitrix\Main\Context::getCurrent()->getSite());

        $noExists = true;

        foreach ($basket as $basketItem) {
            if ($basketItem->getField('PRODUCT_ID') == $productId && !$basketItem->isDelay()) {
                $noExists = false;
                $basketItem->setField('QUANTITY', $basketItem->getQuantity() + $quantity);
            }
        }

        if ($noExists) {
            $item = $basket->createItem('catalog', $productId);
            $elementInfo = \CIBlockElement::GetByID($productId)->fetch();
            $item->setFields(array(
                'QUANTITY' => $quantity,
                'DELAY' => 'N',
                'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
                'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
                'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                'PRODUCT_XML_ID' => $elementInfo['EXTERNAL_ID'],
                'CATALOG_XML_ID' => $elementInfo['IBLOCK_EXTERNAL_ID']
            ));
        }

        $basket->save();

        return true;

    }

    /**
     * @param $productId
     * @param int $quantity
     */
    public static function update(
        $productId,
        $quantity = 1)
    {

        if(empty($quantity))
            $quantity = 1;

        $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(),
            \Bitrix\Main\Context::getCurrent()->getSite());

        foreach ($basket as $basketItem) {
            if ($basketItem->getField('PRODUCT_ID') == $productId && !$basketItem->isDelay()) {
                $basketItem->setField('QUANTITY', $quantity);
            }
        }
        $basket->save();

        return true;
    }

    /**
     * Метод добавления добавления товара в отложенные
     * @param int $productId
     * @return bool
     */
    public static function delay($productId)
    {

        $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(),
            \Bitrix\Main\Context::getCurrent()->getSite());

        $item = $basket->createItem('catalog', $productId);
        $elementInfo = \CIBlockElement::GetByID($productId)->fetch();
        $item->setFields(array(
            'QUANTITY' => '1',
            'DELAY' => 'Y',
            'CURRENCY' => \Bitrix\Currency\CurrencyManager::getBaseCurrency(),
            'LID' => \Bitrix\Main\Context::getCurrent()->getSite(),
            'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
            'PRODUCT_XML_ID' => $elementInfo['EXTERNAL_ID'],
            'CATALOG_XML_ID' => $elementInfo['IBLOCK_EXTERNAL_ID']
        ));

        $basket->save();

        return true;

    }

    /**
     * Метод добавления удаления товара из корзины
     * @param int $productId
     * @return bool
     */
    public static function delete($productId)
    {

        $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(),
            \Bitrix\Main\Context::getCurrent()->getSite());

        foreach ($basket as $basketItem) {
            if ($basketItem->getField('PRODUCT_ID') == $productId && !$basketItem->isDelay()) {
                $basket->getItemById($basketItem->getId())->delete();
            }
        }

        $basket->save();

        return true;

    }

    /**
     * Метод добавления удаления товара из отложенных
     * @param int $productId
     * @return bool
     */
    public static function deleteDelay($productId)
    {
        $basket = \Bitrix\Sale\Basket::loadItemsForFUser(\Bitrix\Sale\Fuser::getId(),
            \Bitrix\Main\Context::getCurrent()->getSite());

        foreach ($basket as $basketItem) {
            if ($basketItem->getField('PRODUCT_ID') == $productId && $basketItem->isDelay()) {
                $basket->getItemById($basketItem->getId())->delete();
            }
        }

        $basket->save();

        return true;

    }

}