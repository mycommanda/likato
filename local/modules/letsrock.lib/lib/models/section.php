<?php

namespace Letsrock\Lib\Models;

use Bitrix\Main\Loader;

Loader::includeModule('iblock');

/**
 * Класс для работы с разделами инфоблока
 * Class Section
 * @package Letsrock\Lib\Models
 */
class Section
{

    /**
     * Метод для получения списка разделов инфоблока
     * @param $iblock
     * @param array $select
     * @param $filter
     * @param array $order
     * @return array
     */
    public static function getList(
        $iblock,
        $select = array('ID', 'NAME', 'CODE'),
        $filter = array('ACTIVE' => 'Y'),
        $order = array('SORT' => 'ASC')
    ) {

        $filter['IBLOCK_ID'] = $iblock;

        $rsSect = \CIBlockSection::GetList($order, $filter, false, $select);
        $data = array();;
        while ($arSect = $rsSect->GetNext()) {
            $data[] = $arSect;
        }

        return $data;
    }
}
