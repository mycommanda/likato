<?php
/**
 * Пользовательские константы
 */

const P_ASSETS = '/assets/';
const CATALOG_HOME_DIRS = [
	'/catalog/hear/',
	'/catalog/body/',
];
const MAX_PRODUCTS_IN_SECTIONS = 8;


/**
 * Highload блоки
 */

//const HL_CITY = 3;

/**
 * Инфоблоки
 */

const IB_SLIDER_MAIN = 5;
const IB_CATALOG = 6;
const IB_BLOG = 7;
const IB_PROPS = 8;

