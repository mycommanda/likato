<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$APPLICATION->SetTitle($arResult['NAME']);


foreach ($arResult['ITEMS'] as $key => $item) {
	if (!empty($item["PREVIEW_PICTURE"])) {
		$arResult['ITEMS'][$key]['PREVIEW_PICTURE'] = CFile::ResizeImageGet($item["PREVIEW_PICTURE"]['ID'],
			["width" => 500, "height" => 500], BX_RESIZE_IMAGE_PROPORTIONAL);
	} else {
		$arResult['ITEMS'][$key]['PREVIEW_PICTURE']['src'] = P_ASSETS . 'img/empty.png';
	}
}


