<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
$this->setFrameMode(true);

$bxajaxid = CAjax::GetComponentID($component->__name, $component->__template->__name,
	$component->arParams['AJAX_OPTION_ADDITIONAL']);


if ($APPLICATION->GetCurPage(false) == "/catalog/hear/") {
	$topText = \Letsrock\Lib\Property::getText('verkhnee-opisanie-ukhod-za-volosami');
	$topImage = \Letsrock\Lib\Property::getImage('verkhnee-opisanie-ukhod-za-volosami')['src'];
	$bottomText = \Letsrock\Lib\Property::getText('nizhnee-opisanie-ukhod-za-volosami');
	$bottomImage = \Letsrock\Lib\Property::getImage('nizhnee-izobrazhenie-ukhod-za-volosami')['src'];
	$bottomImageInText = \Letsrock\Lib\Property::getImage('nizhnee-opisanie-ukhod-za-volosami')['src'];
	$textAfterProducts = 'ТОВАРЫ ПО УХОДУ ЗА ВОЛОСАМИ';
} else {
	$topText = \Letsrock\Lib\Property::getText('verkhnee-opisanie-ukhod-za-telom');
	$topImage = \Letsrock\Lib\Property::getImage('verkhnee-opisanie-ukhod-za-telom')['src'];
	$bottomText = \Letsrock\Lib\Property::getText('nizhnee-opisanie-ukhod-za-telom');
	$bottomImage = \Letsrock\Lib\Property::getImage('nizhnee-izobrazhenie-ukhod-za-telom')['src'];
	$bottomImageInText = \Letsrock\Lib\Property::getImage('nizhnee-opisanie-ukhod-za-telom')['src'];
	$textAfterProducts = 'ТОВАРЫ ПО УХОДУ ЗА ТЕЛОМ';
}

if (!$_GET["bxajaxid"]):

if (!in_array($APPLICATION->GetCurPage(false), CATALOG_HOME_DIRS)): ?>
	<div class="welcome">
		<div class="welcome__content">
			<h2 class="welcome__title"><?= $arResult['NAME'] ?></h2>
			<span class="caption"><?= $arResult['UF_DESCRIPTION'] ?></span>
			<div class="content"><?= $arResult['DESCRIPTION'] ?></div>
		</div>
		<div class="welcome__image"><img src="<?= $arResult['PICTURE']['SRC'] ?>"></div>
	</div>
<? else: ?>
	<div class="welcome">
		<div class="welcome__content">
			<h2 class="welcome__title"><?= $arResult['NAME'] ?></h2>
			<div class="content"><?= $topText ?></div>
		</div>
		<div class="welcome__image"><img src="<?= $topImage ?>">
		</div>
	</div>
	<div class="info">
		<div class="info__image"><img src="<?= $bottomImage ?>">
		</div>
		<div class="info__content">
			<div class="info__text">
				<div class="content"><?= $bottomText ?></div>
			</div>
			<img src="<?= $bottomImageInText ?>">
		</div>
	</div>
	<? $APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list",
		"",
		[
			"IBLOCK_TYPE"        => $arParams["IBLOCK_TYPE"],
			"IBLOCK_ID"          => $arParams["IBLOCK_ID"],
			"SECTION_ID"         => $arResult['ID'],
			"SECTION_FIELDS"     => ["ID", "CODE", "UF_DESCRIPTION", "PICTURE"],
			"CACHE_TYPE"         => $arParams["CACHE_TYPE"],
			"CACHE_TIME"         => $arParams["CACHE_TIME"],
			"CACHE_GROUPS"       => $arParams["CACHE_GROUPS"],
			"COUNT_ELEMENTS"     => $arParams["SECTION_COUNT_ELEMENTS"],
			"TOP_DEPTH"          => $arParams["SECTION_TOP_DEPTH"],
			"SECTION_URL"        => $arResult["FOLDER"] . $arResult["URL_TEMPLATES"]["section"],
			"VIEW_MODE"          => $arParams["SECTIONS_VIEW_MODE"],
			"SHOW_PARENT_NAME"   => $arParams["SECTIONS_SHOW_PARENT_NAME"],
			"HIDE_SECTION_NAME"  => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
			"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
		],
		$component,
		["HIDE_ICONS" => "Y"]
	); ?>
	<span class="caption caption_section"><?= $textAfterProducts ?></span>
<? endif; ?>
<? if (!empty($arResult['UF_ADDITIONAL_TEXT'])): ?>
	<p class="centred-text"><?= $arResult['UF_ADDITIONAL_TEXT'] ?></p>
<? endif; ?>
<div class="grid js-grid">
	<? endif; ?>
	<? foreach ($arResult['ITEMS'] as $item): ?>
		<a class="item js-item" href="<?= $item['DETAIL_PAGE_URL'] ?>">
		<span class="item__img">
			<img src="<?= $item['PREVIEW_PICTURE']['src'] ?>"/>
		</span>
			<span class="item__title"><?= $item['NAME'] ?></span>
			<? if (!empty($item['PROPERTIES']['PRICE_AND_VALUE']['VALUE'])): ?>
				<span class="item__price"><?= $item['PROPERTIES']['PRICE_AND_VALUE']['VALUE'][0] ?> р.</span>
			<? endif; ?>
		</a>
	<? endforeach; ?>
	<? if ($arResult["NAV_RESULT"]->nEndPage > 1 && $arResult["NAV_RESULT"]->NavPageNomer < $arResult["NAV_RESULT"]->nEndPage): ?>
		<div class="additional-see js-add-btn" id="btn_<?= $bxajaxid ?>">
			<a class="additional-see__btn btn" data-ajax-id="<?= $bxajaxid ?>"
			   href="javascript:void(0)"
			   data-show-more="<?= $arResult["NAV_RESULT"]->NavNum ?>"
			   data-next-page="<?= ($arResult["NAV_RESULT"]->NavPageNomer + 1) ?>"
			   data-max-page="<?= $arResult["NAV_RESULT"]->nEndPage ?>">Показать еще</a>
		</div>
	<? endif ?>
	<? if (!$_GET["bxajaxid"]): ?>
</div>

<? endif; ?>
