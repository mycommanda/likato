<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
$this->setFrameMode(true);

if (!in_array($APPLICATION->GetCurPage(false), CATALOG_HOME_DIRS)) {
	return 0;
}
?>
<div class="grid">
	<? foreach ($arResult['SECTIONS'] as $item): ?>
		<? if ($item['DEPTH_LEVEL'] == 2): ?>
			<a class="grid__item" href="/catalog/<?= $item['CODE'] ?>/" style="background-image: url('<?= $item['PICTURE']['SRC'] ?>');">
				<p class="grid__item-title">
					<strong><?= $item['NAME'] ?></strong><span><?= $item['UF_DESCRIPTION'] ?></span>
				</p>
			</a>
		<? endif; ?>
	<? endforeach; ?>
</div>