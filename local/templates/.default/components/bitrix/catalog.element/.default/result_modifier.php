<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$APPLICATION->SetTitle($arResult['NAME']);
$sectionId = $arResult['IBLOCK_SECTION_ID'];

$arSelect = [
	"ID",
	"IBLOCK_ID",
	'UF_BG',
];
$arFilter = ["ID" => $sectionId, "IBLOCK_ID" => IntVal(IB_CATALOG), "ACTIVE" => "Y", 'depth_level' => '2'];
$res = CIBlockSection::GetList(["SORT" => "ASC"], $arFilter, false, $arSelect);

$arFields = $res->Fetch();

$arResult['BG'] = CFile::ResizeImageGet($arFields["UF_BG"],
	["width" => 500, "height" => 500], BX_RESIZE_IMAGE_PROPORTIONAL);

if (!empty($arResult['DETAIL_PICTURE'])) {
	$arResult['DETAIL_PICTURE'] = CFile::ResizeImageGet($arResult["DETAIL_PICTURE"]['ID'],
		["width" => 500, "height" => 500], BX_RESIZE_IMAGE_PROPORTIONAL);
} else {
	$arResult['DETAIL_PICTURE']['src'] = P_ASSETS . 'img/empty.png';
}

