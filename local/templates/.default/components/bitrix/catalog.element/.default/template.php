<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */
$this->setFrameMode(true);
?>
<div class="product">
	<div class="product__image" style="background-image: url('<?=$arResult['BG']['src']?>');"><img src="<?= $arResult['DETAIL_PICTURE']['src'] ?>"/>
	</div>
	<div class="product__info"><span class="caption"><?= $arResult['NAME'] ?></span>
		<div class="product__image product__image_mob" style="background-image: url('<?=$arResult['BG']['src']?>');"><img src="<?= $arResult['DETAIL_PICTURE']['src'] ?>"/>
		</div>
		<div class="content">
			<p><?= $arResult['PREVIEW_TEXT'] ?></p>
		</div>
		<div class="choose">
			<? if (count($arResult['PROPERTIES']['PRICE_AND_VALUE']['VALUE']) != 1):?>

			<span class="choose__name">Объем:</span>
			<ul class="choose__list">
				<? foreach ($arResult['PROPERTIES']['PRICE_AND_VALUE']['VALUE'] as $key => $item): ?>
					<li class="choose__item" data-price1="<?= $item ?>" data-price2="<?= $item * ((100 - intval(\Letsrock\Lib\Property::getText('skidka-dlya-chlenov-kluba'))) / 100) ?>">
						<span><?= $arResult['PROPERTIES']['PRICE_AND_VALUE']['DESCRIPTION'][$key] ?></span>
					</li>
				<? endforeach; ?>
			</ul>
			<? else: ?>
				<span class="choose__name">Объем: <?= $arResult['PROPERTIES']['PRICE_AND_VALUE']['DESCRIPTION'][0] ?></span>
				<ul class="choose__list" style="display: none;">
					<? foreach ($arResult['PROPERTIES']['PRICE_AND_VALUE']['VALUE'] as $key => $item): ?>
						<li class="choose__item" data-price1="<?= $item ?>" data-price2="<?= $item * ((100 - intval(\Letsrock\Lib\Property::getText('skidka-dlya-chlenov-kluba'))) / 100) ?>">
							<span><?= $arResult['PROPERTIES']['PRICE_AND_VALUE']['DESCRIPTION'][$key] ?></span>
						</li>
					<? endforeach; ?>
				</ul>
			<? endif; ?>
			<div class="choose__price">
				<div class="choose__value" data-price1="data-price1">
					<span class="choose__value-d"><strong></strong><span> руб.</span></span>
				</div>
				<div class="choose__value choose__value_club" data-price2="data-price2"><span class="choose__value-t">для членов клуба</span><span
							class="choose__value-d"><strong></strong><span> руб.</span></span>
				</div>
				<button class="choose__btn">вступить в клуб
				</button>
			</div>
		</div>
		<div class="content">
			<? if (!empty($arResult['PROPERTIES']['COMMON_COMPONENTS']['VALUE'])): ?>
				<p><b>Главные компоненты:</b> <?= $arResult['PROPERTIES']['COMMON_COMPONENTS']['VALUE'] ?></p>
			<? endif; ?>
			<p><b>Описание</b></p>
			<p><?= $arResult['DETAIL_TEXT'] ?></p>
			<? if (!empty($arResult['PROPERTIES']['INSIDE']['VALUE'])): ?>
				<p><b>Состав:</b> <i><?= $arResult['PROPERTIES']['INSIDE']['VALUE'] ?></i></p>
			<? endif; ?>
		</div>
	</div>
</div>

