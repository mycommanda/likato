<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<div class="grid">
	<? foreach ($arResult as $item): ?>
	<a class="grid__item" href="/catalog/<?=$item['CODE']?>/" style="background-image: url('<?= $item['PICTURE']['src'] ?>');">
		<p class="grid__item-title"><strong><?= $item['NAME'] ?></strong><span><?= $item['UF_DESCRIPTION'] ?></span>
		</p>
	</a>
	<? endforeach; ?>
</div>