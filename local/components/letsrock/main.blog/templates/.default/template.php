<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

if (empty($arResult)) {
	return 0;
}

$bigCard = array_shift($arResult);
?>
<span class="caption caption_section">Блог</span>
<div class="grid grid_blog">
	<div class="grid__main">
		<a class="item-blog" href="#" style="background-image: url('<?= $bigCard['PREVIEW_PICTURE']['src'] ?>');">
				<span class="item-blog__info">
					<span class="item-blog__title"><?= $bigCard['NAME'] ?></span>
	                <p class="item-blog__text"><?= $bigCard['PREVIEW_TEXT'] ?></p>
				</span>
		</a>
	</div>
	<? if (!empty($arResult)): ?>
		<div class="grid__list">
			<? foreach ($arResult as $key => $item): ?>
				<a class="item-blog" href="#" style="background-image: url('<?= $item['PREVIEW_PICTURE']['src'] ?>');">
				<span class="item-blog__info">
					<span class="item-blog__title"><?= $item['NAME'] ?></span>
	                <p class="item-blog__text"><?= $item['PREVIEW_TEXT'] ?></p>
				</span>
				</a>
			<? endforeach; ?>
		</div>
	<? endif; ?>
</div>