<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<div class="slider">
	<ul class="slider__list owl-carousel">
		<? foreach ($arResult as $item): ?>
			<li class="slider__item">
				<a class="slider__link" href="<?=(empty($item['PROPERTY_UF_LINK_VALUE']))? "javascript:void(0);":$item['PROPERTY_UF_LINK_VALUE'];?>"><img src="<?=$item['DETAIL_PICTURE']['src']?>"/></a>
			</li>
		<? endforeach; ?>
	</ul>
</div>