<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<div class="cat-index">
	<? foreach ($arResult as $item): ?>
		<a class="cat-index__item" href="/catalog/<?=$item['CODE']?>/">
			<span class="cat-index__title"><?= $item['NAME'] ?></span>
			<img class="cat-index__image" src="<?= $item['PICTURE']['src'] ?>" alt="" role="presentation"/>
		</a>
	<? endforeach; ?>
</div>