<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class MainBigSectionComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$result = [
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
		];
		return $result;
	}

	public function executeComponent()
	{
		if ($this->startResultCache()) {
			$arSelect = [
				"ID",
				"IBLOCK_ID",
				"IBLOCK_SECTION_ID",
				"NAME",
				"DESCRIPTION",
				"LIST_PAGE_URL",
				'PICTURE'
			];
			$arFilter = ["IBLOCK_ID" => IntVal(IB_CATALOG), "ACTIVE" => "Y", 'depth_level' => '1'];
			$res = CIBlockSection::GetList(["SORT" => "ASC"], $arFilter, false, $arSelect);
			$arFields = [];

			while ($ob = $res->GetNextElement()) {
				$arFields[] = $ob->GetFields();
			}

			foreach ($arFields as $key => $row) {

				$arFields[$key]['PICTURE'] = CFile::ResizeImageGet($row["PICTURE"],
					["width" => 1000, "height" => 1000], BX_RESIZE_IMAGE_PROPORTIONAL);
			}

			$this->arResult = $arFields;
			$this->includeComponentTemplate();
		}

		return $this->arResult;
	}
} ?>