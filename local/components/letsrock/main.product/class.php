<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

class MainProductComponent extends CBitrixComponent
{
	public function onPrepareComponentParams($arParams)
	{
		$result = [
			"CACHE_TYPE" => $arParams["CACHE_TYPE"],
			"CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
		];
		return $result;
	}

	public function executeComponent()
	{
		if ($this->startResultCache()) {
			CModule::IncludeModule("iblock");
			$arSelect = [
				"ID",
				"NAME",
				"DETAIL_PICTURE",
				"PREVIEW_TEXT",
				"PROPERTY_SHOW_ON_MAIN",
				"PROPERTY_PRICE_AND_VALUE",
				"PROPERTY_COMMON_COMPONENTS",
				"IBLOCK_SECTION_ID",
			];
			$arFilter = [
				"IBLOCK_ID"              => IntVal(IB_CATALOG),
				"ACTIVE_DATE"            => "Y",
				"ACTIVE"                 => "Y",
				'!PROPERTY_SHOW_ON_MAIN' => false,
			];

			$res = CIBlockElement::GetList(["SORT" => "ASC"], $arFilter, false, ["nPageSize" => 1], $arSelect);
			$arFields = $res->Fetch();

			$arFields['DETAIL_PICTURE'] = CFile::ResizeImageGet($arFields["DETAIL_PICTURE"],
				["width" => 600, "height" => 600], BX_RESIZE_IMAGE_PROPORTIONAL);
			$arSelect = [
				"ID",
				"NAME",
				"UF_DESCRIPTION",
			];
			$arFilter = ["IBLOCK_ID"   => IntVal(IB_CATALOG),
			             "ACTIVE"      => "Y",
			             'ID'          => $arFields['IBLOCK_SECTION_ID'],
			];

			$res = CIBlockSection::GetList(["SORT" => "ASC"], $arFilter, false, $arSelect);
			$section = $res->Fetch();
			$arFields['SECTION_DESCRIPTION'] = $section['UF_DESCRIPTION'];

			$this->arResult = $arFields;
			$this->includeComponentTemplate();
		}

		return $this->arResult;
	}
} ?>