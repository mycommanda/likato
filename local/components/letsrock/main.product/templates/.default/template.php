<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<div class="promo">
	<div class="promo__main-image"><img src="<?=P_ASSETS?>img/magic-oil.jpg"/>
	</div>
	<div class="promo__info">
		<div class="promo__title"><span class="caption">Волшебное масло Magic Oil</span><span>Структурное восстановление</span>
		</div>
		<div class="promo__content content">
			<p>Magic Oil формула с высокомолекулярными кислородосодержащими соединениями и антистатиками</p>
			<p>
				Профессиональное масло для создания мгновенного сэлфи-эффекта роскошных волос обеспечивает термо-защиту при укладке феном, утюжком или плойкой,
				облегчает укладку, предотвращает пушение волос.
				Рекомендуется как средство ежедневной защиты волос от негативной городской среды. Придает волосам гламурный блеск и шелковистую текстуру. Масло облегчает распутывание, снижает механическое повреждение волос при расчесывании и укладке
			</p>
			<p>ОБЪЕМ: 100 мл</p>
			<p>ОСНОВНЫЕ КОМПОНЕНТЫ: ВЫСОКОМОЛЕКУЛЯРНЫЕ КИСЛОРОДОСОДЕРЖАЩИЕ СОЕДИНЕНИЯ И АНТИСТАТИКИ</p>
		</div>
		<div class="promo__image promo__image_pos--right-bottom"><img src="<?=P_ASSETS?>img/girl-with-rascheska.png"/>
		</div>
	</div>
</div>