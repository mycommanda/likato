<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
<div class="promo">
	<div class="promo__main-image"><img src="<?= $arResult['DETAIL_PICTURE']['src'] ?>"/>
	</div>
	<div class="promo__info">
		<div class="promo__title">
			<span class="caption"><?= $arResult['NAME'] ?></span><span><?= $arResult['SECTION_DESCRIPTION'] ?></span>
		</div>
		<div class="promo__content content">
			<p><?= $arResult['PREVIEW_TEXT'] ?></p>
			<? if (!empty($arResult['PROPERTY_PRICE_AND_VALUE_DESCRIPTION'])): ?>
				<p>ОБЪЕМ: <?= implode(', ', $arResult['PROPERTY_PRICE_AND_VALUE_DESCRIPTION']) ?></p>
			<? endif; ?>
			<? if (!empty($arResult['PROPERTY_COMMON_COMPONENTS_VALUE'])): ?>
				<p>ОСНОВНЫЕ КОМПОНЕНТЫ: <?= $arResult['PROPERTY_COMMON_COMPONENTS_VALUE'] ?></p>
			<? endif; ?>
		</div>
		<div class="promo__image promo__image_pos--right-bottom"><img src="/assets/img/girl-with-rascheska.png"/>
		</div>
	</div>
</div>