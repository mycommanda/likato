<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}
?>
	</div>
	<footer class="footer footer">
		<div class="footer__container container container_flex"><span>&copy; 2019 Likato. Все права защищены </span>
		</div>
	</footer>
	<script src="/assets/js/libs.js"></script>
	<script src="/assets/js/script.js"></script>
	<script src="/assets/js/maps.js"></script>
</body>
</html>