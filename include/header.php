<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<? $APPLICATION->ShowHead(); ?>
	<link rel="stylesheet" href="<?= P_ASSETS ?>css/style.css"/>
	<link rel="stylesheet" href="<?= P_ASSETS ?>css/lets.css"/>
	<link rel="icon" type="image/x-icon" href="favicon.ico"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

	<title><? $APPLICATION->ShowTitle() ?></title>
</head>
<body class="page <?= ($APPLICATION->GetCurPage(false) == "/") ? "" : "page_colorito"; ?>">
<? $APPLICATION->ShowPanel() ?>
<header class="page__header header <?= ($APPLICATION->GetCurPage(false) == "/") ? "header_index" : ""; ?>">
	<div class="header__container container">
		<button class="menu" data-open="menu">
			<div class="menu__toggle" data-open-toggle="data-open-toggle">
				<svg>
					<use xlink:href="/assets/img/symbols.svg#svg-menu"></use>
				</svg>
				<svg>
					<use xlink:href="/assets/img/symbols.svg#svg-menu-close"></use>
				</svg>
			</div>
		</button>
		<a class="logotype" href="/"><span class="logotype__img">
        <svg>
          <use xlink:href="/assets/img/symbols.svg#svg-logo"></use>
        </svg></span>
			<p>природа, усиленная наукой</p></a>
		<div class="header__wrap" data-show="menu">
			<? $APPLICATION->IncludeComponent("bitrix:menu", "header", [
				"ROOT_MENU_TYPE"  => "top",    // Тип меню для первого уровня
				"MAX_LEVEL"       => "1",    // Уровень вложенности меню
				"CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
				"USE_EXT"         => "Y",    // Подключать файлы с именами вида .тип_меню.menu_ext.php
			],
				false
			); ?>
			<a class="phone" href="tel:88005114501">8 800 511 45 01</a>
		</div>
	</div>
</header>